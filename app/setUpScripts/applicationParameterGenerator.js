/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let assetConfigModel = require('../models/applicationParameter');

let db = mongoose.connection;
mongoose.connect(config.db,function(){
    console.log("MongoDb Connected On  "+config.db);
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let sensorConfiguration = [
    {
        "assetType" :{
            "typeId":"VehicleType",
            "label":"VehicleType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "vehicleTypeName" : {
                "type" : "text",
                "field":"vehicleTypeName",
                "description" : "Enter vehicle Type Name",
                "label":"vehicle Type Name",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "noofSeatsAvailable" : {
                "type" : "number",
                "field":"noofSeatsAvailable",
                "description" : "Enter No of Seats available",
                "label":"No of Seats available",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"number"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"LessorType",
            "label":"LessorType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "lessorType" : {
                "type" : "text",
                "field":"lessorType",
                "description" : "Enter Lessor Type",
                "label":"Lessor Type",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "LessorTypeDesp" : {
                "type" : "text",
                "field":"LessorTypeDesp",
                "description" : "Enter Lessor Type Description ",
                "label":"Lessor Type Description",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"ShiftTypeCost",
            "label":"ShiftTypeCost",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "ShiftTypeName" : {
                "type" : "text",
                "field":"ShiftTypeName",
                "description" : "Enter Shift Type Name",
                "label":"Shift Type Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "ShiftTypeDesp" : {
                "type" : "text",
                "field":"ShiftTypeDesp",
                "description" : "Enter Shift Type Description ",
                "label":"Shift Type Description",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"ShiftTypeCost",
            "label":"ShiftTypeCost",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "expenseTypeName" : {
                "type" : "text",
                "field":"expense Type Name",
                "description" : "Enter Expense Type Name",
                "label":"Expense Type Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "expenseTypeDesp" : {
                "type" : "text",
                "field":"expense Type Desp",
                "description" : "Enter expense Type Description ",
                "label":"Expense Type Description",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"FuelType",
            "label":"FuelType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "fuelTypeName" : {
                "type" : "text",
                "field":"fuelTypeName",
                "description" : "Enter Fuel Type Name",
                "label":"Fuel Type Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "fuelTypeDesp" : {
                "type" : "text",
                "field":"fuelTypeDesp",
                "description" : "Enter Fuel Type Description ",
                "label":"Fuel Type Description",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"ServiceType",
            "label":"ServiceType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "serviceTypeName" : {
                "type" : "text",
                "field":"serviceTypeName",
                "description" : "Enter Service Type Name",
                "label":"Service Type Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "serviceTypeDesp" : {
                "type" : "text",
                "field":"serviceTypeDesp",
                "description" : "Enter Service Type Description ",
                "label":"Service Type Description",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"ExpenseType",
            "label":"ExpenseType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "ExpenseTypeName" : {
                "type" : "text",
                "field":"ExpenseTypeName",
                "description" : "Enter Expense Type Name",
                "label":"Expense Type Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "ExpenseTypeDesp" : {
                "type" : "text",
                "field":"ExpenseTypeDesp",
                "description" : "Enter Expense Type Description ",
                "label":"Expense TypeDescription",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"corporateContractType",
            "label":"corporateContractType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "corporateContractName" : {
                "type" : "text",
                "field":"corporateContractName",
                "description" : "Enter corporate ContractType Name",
                "label":"corporate Contract  Name",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "corporateContractDesp" : {
                "type" : "text",
                "field":"corporateContractDesp",
                "description" : "Enter corporate Contract Description ",
                "label":"corporate Description",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"vechicleHistoryType",
            "label":"vechicleHistoryType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },


        "configuration" : {
            "vechicleEvents" : {
                "type" : "text",
                "field":"vechicleEvents",
                "description" : "Enter vechicle Events",
                "label":"vechicle Events",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            },
            "vechicleEventName" : {
                "type" : "text",
                "field":"vechicleEventName",
                "description" : "Enter vechicle Event Name",
                "label":"vechicle Event Name",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "costOfEvent" : {
                "type" : "text",
                "field":"costOfEvent",
                "description" : "Enter cost Of Event ",
                "label":"Event Cost",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "eventDate" : {
                "type" : "text",
                "field":"eventDate",
                "description" : "Select Event Date",
                "label":"Event Date",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"date"
            }
        }
    },
    {
        "assetType": {
            "typeId": "userConfig",
            "label": "userConfig",
            theme: {
                color: {
                    primary: "#5C6BC0",
                    secondary: "#7986CB"
                }
            },
            order: 1
        },
        "configuration": {
            "username": {
                "type": "text",
                "field": "username",
                "description": "Enter username",
                "label": "user Name",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "email": {
                "type": "text",
                "field": "email",
                "description": "Enter email",
                "label": "Email",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "password": {
                "type": "password",
                "field": "password",
                "description": "Enter password",
                "label": "Password",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "password"
            },
            "repeatPassword": {
                "type": "password",
                "field": "repeatPassword",
                "description": "Enter repeat Password",
                "label": "Repeat Password",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "password"
            },

            "firstName": {
                "type": "text",
                "field": "firstName",
                "description": "Enter firstName",
                "label": "First Name",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "middleName": {
                "type": "text",
                "field": "middleName",
                "description": "Enter middleName",
                "label": "Middle Name",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "lastName": {
                "type": "text",
                "field": "lastName",
                "description": "Enter lastName",
                "label": "Last Name",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "phoneNo": {
                "type": "text",
                "field": "phoneNo",
                "description": "Enter phoneNo",
                "label": "Phone Number",
                "required": true,
                "autoComplete": false,
                "fieldDisplayType": "text"
            },
            "role" : {
                "field": "role",
                "type": "dropDown",
                "description": "Select Role",
                "label": "Role",
                "required": false,
                "fieldDisplayType": "dropDown",
                "autoComplete": true,
                "value": {
                    "dropDownValues": ["admin","user","manager"],
                    "defaultValue": "active"

                }
            },
            "status" : {
                "field":"status",
                "type" : "dropDown",
                "description" : "Select Status",
                "label":"Status",
                "required":false,
                "fieldDisplayType":"dropDown",
                "autoComplete":true,
                "value":{
                    "dropDownValues": ["active","inactive"],
                    "defaultValue":"active"
                }
            }
        }
    },

    {
        "assetType" :{
            "typeId":"VehicleTransactionType",
            "label":"VehicleTransactionType",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
        /*    "vechicleNo" : {
                "type" : "number",
                "field":"vechicleNo",
                "description" : "Enter vechicleNo",
                "label":"vechicle No",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },*/
            "tSno" : {
                "type" : "number",
                "field":"TSno",
                "description" : "Enter TS no",
                "label":"TS No",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "transactionDate" : {
                "type" : "text",
                "field":"transactionDate",
                "description" : "Enter transactionDate",
                "label":"Transaction Date",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"date"
            },
            /*
            "mode" : {
                "type" : "number",
                "field":"mode",
                "description" : "Enter mode",
                "label":"mode",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },*/


            "bookedBy" : {
                "type" : "bookedBy",
                "field":"bookedBy",
                "description" : "Enter bookedBy",
                "label":"Booked By",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },


            "totalHours" : {
                "type" : "number",
                "field":"totalHours",
                "description" : "Enter total Hours",
                "label":"Total Hours",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },


            "totalKms" : {
                "type" : "number",
                "field":"totalKms",
                "description" : "Enter total Kms",
                "label":"Total Kms",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },


         /*   "extraHours" : {
                "type" : "number",
                "field":"extraHours",
                "description" : "Enter extra Hours",
                "label":"Extra Hours",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "extraKms" : {
                "type" : "number",
                "field":"extraKms",
                "description" : "Enter extra Kms",
                "label":"Extra Kms",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "ratePerHr" : {
                "type" : "number",
                "field":"ratePerHr",
                "description" : "Enter rate Per Hr",
                "label":"Rate Per Hr",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "ratePerKm" : {
                "type" : "number",
                "field":"ratePerKm",
                "description" : "Enter rate Per Km",
                "label":"Rate Per Km",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },

            "slabRate" : {
                "type" : "number",
                "field":"slabRate",
                "description" : "Enter slab Rate",
                "label":"Slab Rate",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },*/

        /*    "extraAmtPerHr" : {
                "type" : "number",
                "field":"extraAmtPerHr",
                "description" : "Enter extra Amt Per Hr",
                "label":"Extra Amt Per Hr",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },


            "extraKmAmt" : {
                "type" : "number",
                "field":"extraKmAmt",
                "description" : "Enter extra Km Amt",
                "label":"Extra Km Amt",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },

            "extraAmtPerHr" : {
                "type" : "number",
                "field":"extraAmtPerHr",
                "description" : "Enter extraAmtPerHr",
                "label":"Extra Amt Per Hr",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "tripAmt" : {
                "type" : "number",
                "field":"tripAmt",
                "description" : "Enter tripAmt",
                "label":"Trip Amt",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            }*/

        }
    },


];
function generateSensorConfiguration(){
    assetConfigModel.insertMany(sensorConfiguration,function(err,res){
        console.log("Added "+res.length+" Asset Configuration");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    assetConfigModel.remove({},function(err,result){
        console.log("Deleting previous Asset Config Data");
        generateSensorConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};

