/**
 * Created by suhas on 14/6/18.
 */
let assetsModule = require("./assets"),
    assetConfigModule = require("./assetConfig")
applicationParameterModule = require("./applicaitionParameterConfig")

module.exports={
    assets:assetsModule,
    assetConfig:assetConfigModule,
    applicationParameter:applicationParameterModule
};
